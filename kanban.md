# Ideas

## list-util-replace-last

## list-util-replace-first

## list-util-replace
(list-util-replace (list 1 2 3) 1 4))
> (list 4 2 4)

## list-util-get-from-end
(define x (list 1 2 3 4))
(list-util-get-from-end x 0)
> 4

## list-util-drop
Takes as argument a list and number of items to drop from the end.
Returns list without N items in the end.

(list-util-drop (list 1 2 3) 1)
>(1 2)

## first

## last

# To do

## list-util-sublist error if index out of bounds
It errors anyway but write better error message for this




