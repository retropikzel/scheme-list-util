(define-library
  (retropikzel list-util v0.2.1 main)
  (import (scheme base)
          (scheme write))
  (export list-util-get
          list-util-sublist
          list-util-deep-member)
  (begin

    (define list-util-get
      (lambda (key alist other)
        (let ((pair (assoc key alist)))
          (if pair
            (cdr pair)
            #f))))

    ; Gets inclusive sublist of _list_ by _start-index_ and _end-index_
    (define list-util-sublist
      (lambda (list start-index end-index)
        (reverse (list-tail (reverse (list-tail list start-index))
                            (- (- (length list) end-index) 1)))))

    ; Returns first sublist of _search-from_ whose beginning
    ; is _search-for_
    ;
    ; Uses equal? for comparison
    (define list-util-deep-member
      (lambda (search-for search-from)
        (if (null? search-from)
          #f
          (letrec ((looper (lambda (search-from-car search-from-cdr)
                             (if (and (>= (length search-from-car)
                                          (length search-for))
                                      (equal? search-for
                                              (list-util-sublist search-from-car
                                                                 0
                                                                 (- (length search-for) 1))))
                               search-from-car
                               (if (null? search-from-cdr)
                                 #f
                                 (looper (car search-from-cdr)
                                         (cdr search-from-cdr)))))))
            (looper (car search-from)
                    (cdr search-from))))))))
